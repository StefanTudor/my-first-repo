const FIRST_NAME = "Tudor";
const LAST_NAME = "Stefan";
const GRUPA = "1085";

function numberParser(value) {
    if (typeof (value) == "number" && isFinite(value) === true) {
        if (value <= Number.MIN_SAFE_INTEGER || value >= Number.MAX_SAFE_INTEGER) return NaN;
        else return Math.trunc(value);
    }
    else if (isNaN(parseInt(value) % 2) === false) return parseInt(value); //acopera si cazul in care value == NaN
    else if (isFinite(value) === false) return NaN;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}